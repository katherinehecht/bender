// listen for auth status changes
// if logged out, hide things, if logged in show things
auth.onAuthStateChanged(user => {
  if (user) {

    db.collection('deals').onSnapshot(snapshot => {
      setupVenueBeals(snapshot.docs);
      setupUI(user);
    }, err => console.log(err.message));
  } else {
    setupUI();
    setupVenueBeals([]);
  }
})

//create new beal
const createForm = document.querySelector('#create-beal-form');
createForm.addEventListener('submit', (e) => {
  e.preventDefault();

  db.collection('deals').add({
    name: createForm['name'].value,
    content: createForm['content'].value
  }).then(() =>
  {
    // close the create modal & reset form
    const modal = document.querySelector('#modal-create');
    M.Modal.getInstance(modal).close();
    createForm.reset();
  }).catch(err => {
    console.log(err.message);
  });
});


// signup User
const signupFormUser = document.querySelector('#user-signup-form');
signupFormUser.addEventListener('submit', (e) => {
  //submit automatically happens when click sign up
  //don't want to default to refreshing page
  e.preventDefault();

  // get user info
  const name = signupFormUser['signup-name'].value;
  const email = signupFormUser['signup-email'].value;
  const password = signupFormUser['signup-password'].value;

  // sign up the user with firebase
  auth.createUserWithEmailAndPassword(email, password).then(cred => {
	return db.collection('users').doc(cred.user.uid).set({
	  name: signupFormUser['signup-name'].value
	}); //user just created uid and match name to it
  }).then(() => {
    // close the signup modal & reset form
    const modal = document.querySelector('#modal-user-signup');
    M.Modal.getInstance(modal).close(); //closes popup
    signupFormUser.reset(); //clears signup form
  });
});


// signup Venue
const signupFormVenue = document.querySelector('#venue-signup-form');
signupFormVenue.addEventListener('submit', (e) => {
  //submit automatically happens when click sign up
  //don't want to default to refreshing page
  e.preventDefault();

  // get user info
  const name = signupFormVenue['signup-name'].value;
  const email = signupFormVenue['signup-email'].value;
  const password = signupFormVenue['signup-password'].value;

  // sign up the user with firebase
  auth.createUserWithEmailAndPassword(email, password).then(cred => {
	return db.collection('venue').doc(cred.user.uid).set({
	  name: signupFormVenue['signup-name'].value
	}); //user just created uid and match name to it
  }).then(() => {
    // close the signup modal & reset form
    const modal = document.querySelector('#modal-venue-signup');
    M.Modal.getInstance(modal).close(); //closes popup
    signupForm.reset(); //clears signup form
  });
});

// logout Venue

const logoutVenue = document.querySelector('#logout-venue');
logoutVenue.addEventListener('click', (e) => {
  e.preventDefault();
  auth.signOut();
  const createForm = document.querySelector('#create-form');
  createForm.reset();
});

const logoutUser = document.querySelector('#logout-user');
logoutUser.addEventListener('click', (e) => {
  e.preventDefault();
  auth.signOut();

  const createForm = document.querySelector('#create-form');
  createForm.reset();
});

// login User
const loginFormUser = document.querySelector('#user-login-form');
loginFormUser.addEventListener('submit', (e) => {
  e.preventDefault();

  // get user info
  const email = loginFormUser['login-email'].value;
  const password = loginFormUser['login-password'].value;

  // log the user in
  auth.signInWithEmailAndPassword(email, password).then((cred) => {
    // close the signup modal & reset form
    const modal = document.querySelector('#modal-user-login');
    M.Modal.getInstance(modal).close(); //close popup
    loginFormUser.reset(); //clear login form
  });


});

// login Venue
const loginFormVenue = document.querySelector('#venue-login-form');
loginFormVenue.addEventListener('submit', (e) => {
  e.preventDefault();

  // get user info
  const email = loginFormVenue['login-email'].value;
  const password = loginFormVenue['login-password'].value;

  // log the user in
  auth.signInWithEmailAndPassword(email, password).then((cred) => {
    // close the signup modal & reset form
    const modal = document.querySelector('#modal-venue-login');
    M.Modal.getInstance(modal).close(); //close popup
    loginFormVenue.reset(); //clear login form
  });
});
