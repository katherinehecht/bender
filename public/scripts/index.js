// DOM elements
const bealList = document.querySelector('.deals');
const loggedOutLinks = document.querySelectorAll('.logged-out'); //show certain links depending id logged in
const loggedInLinksUser = document.querySelectorAll('.user-logged-in');
const loggedInLinksVenue= document.querySelectorAll('.venue-logged-in');
const accountDetails = document.querySelector('.account-details');

// USER
const setupUI = (user) => {
  if (user) {

    // toggle user UI elements
    db.collection('users').doc(user.uid).get().then(doc => {
      if(doc.exists){
      	db.collection('users').doc(user.uid).get().then(doc => {
            const html = `
            	<div>Logged in as ${user.email}</div>
              <div>${doc.data().name}</div>
            `;
      	  accountDetails.innerHTML = html;
      	});

      loggedInLinksUser.forEach(item => item.style.display = 'block');
      loggedOutLinks.forEach(item => item.style.display = 'none');
      } 
    });
    db.collection('venue').doc(user.uid).get().then(doc => {
      if(doc.exists){
        db.collection('venue').doc(user.uid).get().then(doc => {
            const html = `
              <div>Logged in as ${user.email}</div>
              <div>${doc.data().name}</div>
            `;
          accountDetails.innerHTML = html;
        });
        loggedInLinksVenue.forEach(item => item.style.display = 'block');
        loggedOutLinks.forEach(item => item.style.display = 'none');
      }
    });
  } else {
    // clear account info
      accountDetails.innerHTML = '';
      // toggle user elements
      loggedInLinksVenue.forEach(item => item.style.display = 'none');
      loggedInLinksUser.forEach(item => item.style.display = 'none');
      loggedOutLinks.forEach(item => item.style.display = 'block');

  }
};

const setupVenueBeals = (data) => {
	
  if (data.length) {
    let html = '';
    data.forEach(doc => {
      const deal = doc.data();
      const li = `
        <li>
          <div class="collapsible-header grey lighten-4"> ${deal.name} </div>
          <div class="collapsible-body white"> ${deal.content} </div>
        </li>
      `;
      html += li;
    });
    bealList.innerHTML = html
  } else {
    bealList.innerHTML = '';
  }
};

// setup materialize components
document.addEventListener('DOMContentLoaded', function() {

  var modals = document.querySelectorAll('.modal');
  M.Modal.init(modals);

  var items = document.querySelectorAll('.collapsible');
  M.Collapsible.init(items);

});

